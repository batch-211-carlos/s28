db.rooms.insertOne({
	name: "single",
	accommodates: 2,
	price: 1000,
	description: "A single room with all the basic necessities",
	rooms_available: 10,
	isAvailable: false
});


db.rooms.insertMany([
	{name: "double", accommodates: 3, price: 2000, description: "A room fit for a small family going on vacation", rooms_available: 5, isAvailable: false},
	{name: "double", accommodates: 4, price: 4000, description: "A room with a queen sized bed perfect for a simple getaway", rooms_available: 15, isAvailable: false}]);

db.rooms.find({ name: "double" });


db.rooms.updateOne(
{ 
	_id: ObjectId("63464c928dd038a71f44b726")
},
{
	$set: {
		rooms_available: 0
	}
}
);


db.rooms.deleteMany({rooms_available: 0})

db.getCollection("rooms").find({});



