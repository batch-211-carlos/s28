//CRUD Operations
/*
- CRUD is an acronym for create, read, update and delete
- Create: the create function allows users to create a new record in the database
- Read: the read function is similar to the search function. It allows users to search and retrieve specific records
- Update: The update function is used to modify existing records that are on our database
- Delete: The delete function allows users to remove records from a database that is no longer needed
*/

//CREATE: INSERT documents
/*
- The mongo shell uses JS for its syntax
 - MongoDB deals with objects as it's structure for documents 
 - We can create documents by providing objects into our methods
  - JS syntax:
  	- object.object.method({object})
*/

//INSERT ONE
/*
-Syntax:
	db.collectionName.insertOne({object})
*/

//Sample
db.users.insert({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "87654321",
		email: "janedoe@gmail.com"
	}
});

//INSERT MANY
/*
-Syntax: 
	db.collectionName.insertMany([ {objectA}, {objectB}])
*/

db.users.inserMany([
{
	firstName: "Stephen",
	lastName: "Hawking",
	age: 76,
	contact:{
		phone: "87654321",
		email: "stephenhawking@gmail.com"
	},
	course: ["Phyton", "React", "PHP"],
	department: "none"
},
{
	firstName: "Neil",
	lastName: "Armstrong",
	age: 82,
	contact:{
		phone: "87654321",
		email: "neilarmstrong@gmail.com"
	},
	course: ["React", "Laravel", "Sass"],
}
]);

//READ: FIND or RETRIEVE Documents
/*
 - The documents will be returned based on their order of storage in the collection
*/

//FIND all documents
/*
Syntax:
	db.collectionName.find();
*/

db.users.find();

//FIND using Single parameter
/*
Syntax:
	db.collectioName.find({field: value})
*/
db.users.find({firstName: "Stephen"});

//Find using multiple parameters
/*
Syntax:
	db.collectionName.find({ fieldA: valueA, fieldB: valueB})
*/

db.users.find({ lastName: "Armstrong", age: 82})

//FIND + Pretty Method
/*
- The "pretty" method allows us to be able to view the documents returned by our terminal in a "prettier" format
Syntax:
	db.collectionName.find({ field: value}).pretty();
*/

db.users.find({ lastName: "Armstrong", age: 82}).pretty();

//UPDATE: EDIT a document

//UPDATE ONE: updating a single document
/*
Syntax:
	db.collectionName.updateOne({criteria}, {$set: {field: value}});
*/
//For our example, let us create a document that we will then update
//1. Insert initial document
db.users.insertOne({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact:{
		phone: "0000000",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
});


//Mini Activity:
/*
1. Change the contents of the document that contains "Test" as its first name using the updateOne. update the document with the below details.
2. Return/view/read the document using the users.find
3. using the pretty method
4. screenshot the returned document and paste it on our hangouts
5. Pass on or before 6:25
*/
//2. Update the document
db.users.updateOne(
	{firstName: "Test"},
	{
		$set: {
		firstName: "Bill",
		lastName: "Gates",
		age: 65,
		contact:{
		phone: "87654321",
			email: "bill@gmail.com"
		},
		courses: ["PHP", "Laravel", "HTML"],
		department: "Operations",
		status: "active"
	}
    }
	);

db.users.find({ firstName: "Bill", age: 65}).pretty();

//Update Many: Updating multiple documents
/*
Syntax:
	db.collectionName.updateMany({criteria}, {$set {field: value}})
*/
db.users.updateMany(
	{department: "none"},
	{
		$set: {department: "HR"}
	}	

	);
db.users.find().pretty();

//REPLACE ONE
/*
- Replace one replaces the whole document
- If updateOne updates the specific fields, replaceOne replaces the whole document
 - If updateOne updates parts, replaceOne replaces tha whole document
*/
db.users.replaceOne(
	{ firstName: "Bill"},
	{
		firstName: "Bill",
		lastName: "Gates",
		age: 65,
		contact: {
			phone: "12345678",
			email: "bill@gmail.com"
		},
		courses: ["PHP", "Laravel", "HTML"],
		department: "Operations"
	}
	);
db.users.find({ firstName: "Bill"});

//DELETE: Deleting documents
//For our example, let us create a document that we will delete 
/*
It is good to practice soft deletion or archiving of our document instead of deleting them or removing them from the system
*/
db.users.insertOne({
	firstName: "test"
});

//DELETE ONE: deleting a single document
/*
Syntax:
	db.collectionName.deleteOne({criteria})
*/
db.users.deleteOne({
	firstName: "test"
});
db.users.find({firstName: "test"}).pretty();

//Delete Many: Delete Many documents
/*
Syntax: 
	db.collectionName.deleteMany({criteria});
*/
db.users.deleteMany({
	firstName: "Bill"
});

//Delete all: Delete all documents
/*
Syntax:
	db.collectionName.deleteMany();
*/

//Soft deletion
//Example, deleted but not on the system
db.users.find({isActive: false, })

//Advance Queries
/*
- Retrieving data with complex data structures is also a good skill for any developer to have
- Real world examples of data can be as complex as having two or more layers of nested objects
- Learning to query these kinds of data is also essential to ensure that we are able to retrieve any information that we would need in our application
*/

//Query an embedded document
//An embedded document are those types of documents that contain a document inside a document.
db.users.find({
	contact: {
		phone: "87654321",
		email: "stephenhawking@gmail.com"
	}
}).pretty();

//Query on nested field
db.users.find({
	"contact.email": "janedoe@gmail.com"
}).pretty();

//Querying an Array with Exact elements
db.users.find({courses: ["CSS", "Javascript", "Phyton"]}).pretty();

//Querying an Array without regard to order
db.users.find({courses: {$all: ["React", "Phyton"]}}).pretty();

//Querying an Embedded Array
db.users.insert({
	nameArr: [
	{
		nameA: "Juan"
	},
	{
		nameB: "Tamad"
	}
	]
});
db.users.find({
	nameArr:
	{
		nameA: "Juan"
	}
}).pretty();